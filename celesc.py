#!/usr/local/bin/python3.6
import json
import urllib.request
from beautifultable import BeautifulTable

table = BeautifulTable()
wp = urllib.request.urlopen("https://celgeoweb.celesc.com.br/json/tabelas.js")
pw = wp.read().decode('utf-8').replace('var visaoGeralPublico =','').replace(']}]}]};',']}]}]}')
res = json.loads(pw)

table.column_headers = ["CIDADE", "QUANTIDADE_TOTAL", "QUANTIDADE_PROGRAMADA" ,"QUANTIDADE_ACIDENTAL"]

for i in res['REGIONAIS']:
	for j in i['CIDADES']:
		table.append_row([j['CIDADE'],j['QUANTIDADE_TOTAL'],j['QUANTIDADE_PROGRAMADA'],j['QUANTIDADE_ACIDENTAL']])

print(table)
