#!/usr/local/bin/python3.6
import json
import urllib.request
from beautifultable import BeautifulTable

wp = urllib.request.urlopen("https://celgeoweb.celesc.com.br/json/tabelas.js")
pw = wp.read().decode('utf-8').replace('var visaoGeralPublico =','').replace(';','')
res = json.loads(pw)

table = BeautifulTable()
table.column_headers = ["BAIRRO", "TOTAL", "PROGRAMADA" ,"ACIDENTAL"]

parent_table = BeautifulTable()
parent_table.rows.append([table])
parent_table.set_style(BeautifulTable.STYLE_SEPARATED)
parent_table.columns.padding_left[0] = 0
parent_table.columns.padding_right[0] = 0

for i in res['REGIONAIS']:
	for j in i['CIDADES']:
		if (j['CIDADE'] == 'Florianopolis'):
			total_cidade=i['QUANTIDADE_TOTAL']
			total_cidade_programada=i['QUANTIDADE_TOTAL']
			total_cidade_acidental=i['QUANTIDADE_TOTAL']

			parent_table.columns.header = [j['CIDADE']]
			for k in j['BAIRROS']:
				table.rows.append([k['BAIRRO'],k['QUANTIDADE_TOTAL'],k['QUANTIDADE_PROGRAMADA'],k['QUANTIDADE_ACIDENTAL']])
table.rows.append(['TOTAL CIDADE',total_cidade,total_cidade_programada,total_cidade_acidental])

print(parent_table)